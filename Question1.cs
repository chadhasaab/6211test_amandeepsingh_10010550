﻿using System;
using System.Collections.Generic;


namespace Question1TestFile
{
    class Question1
    {
        /*------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------Question 1A - Fix the population of the List of unique 4 digit numbers------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/

        private const int size = 100;
        public static Dictionary<int> NumsLst = new List<int>();
        //Generate RandomNo
    public int GenerateRandomNo()
    {
        int _min = 0000;
        int _max = 9999;
        Random _rdm = new Random();
        return _rdm.Next(_min, _max);
    }

        /*----------------------------------------------------------------------------------------------------------------------------------*/
        /*-------- Question 1B - Fix converting the List to array and displaying the contents with the largest and smallest values ---------*/
        /*----------------------------------------------------------------------------------------------------------------------------------*/

       
            static void Main(string[] args)
            {
                int[] array = new int[10];
                Console.WriteLine("enter the array elements to b sorted");
                for (int i = 0; i < 10; i++)
                {
                    array[i] = Convert.ToInt32(Console.ReadLine());
                }
                int smallest = array[0];
                for (int i = 0; i < 10; i++)

                {
                    if (array[i] < smallest)
                    {
                        smallest = array[i];

                    }
                }
                int largest = array[9];
                for (int i = 0; i < 10; i++)
                {

                    if (array[i] > largest)
                    {
                        largest = array[i];

                    }
                }
                Console.WriteLine("the smallest no is {0}", smallest);
                Console.WriteLine("the largest no is {0}", largest);
                Console.Read();


            }
        }
    }
    /*------------------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------------------------Question 1 END-------------------------------------------------------*/
    /*--------------------------------------- NO fixing is required below this point------------------------------------------*/
    /*------------------------------------------------------------------------------------------------------------------------*/






    static void Main(string[] args)
        {
            DisplayTitle(1); //calls method to display Title

            PopulateList();
            DisplayData();

            CompleteExit(); //calls method for exit routine
            Console.ReadLine(); //Readline will wait for an Enter press before closing

        }

        /*------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------- Static methods below are for assistance in displaying etc ----------------------------------*/
        /*---------------------------------------- MAKE NOT CHANGES TO THESE METHODS ---------------------------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/

        public static void DisplayTitle(int num)//This method simply sets up and displays the Title for the question.  It receives the question number as a parameter
        {
            Console.SetWindowSize(51, 40);
            Console.BufferWidth = 51;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" *************************************************");
            Console.WriteLine(" **                                             **");
            Console.Write(" **");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("                  Question{0}                  ", num);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("**");
            Console.WriteLine(" **                                             **");
            Console.WriteLine(" *************************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void CompleteExit()//This method simply sets up and displays the exit routine
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" *************************************************");
            Console.WriteLine(" **                                             **");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(" **          Press ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("ENTER");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" to exit the app.       **");
            Console.WriteLine(" **                                             **");
            Console.WriteLine(" *************************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static double[] DubArray(int size)
        {
            Random rand = new Random();
            double min = 1.01;
            double max = 9.99;
            double[] arr = new double[size];

            for (int i = 0; i < size; i++)
            {
                arr[i] = Math.Round((rand.NextDouble() * (max - min) + min), 2);
            }
            return arr;
        }
    }
}
